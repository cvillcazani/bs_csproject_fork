/*
 @file_controller_test.js Copyright (c) 2021 Jalasoft
 2643 Av Melchor Perez de Olguin , Colquiri Sud, Cochabamba, Bolivia.
 Av. General Inofuentes esquina Calle 20,Edificio Union № 1376, La Paz, Bolivia
 All rights reserved

 This software is the confidential and proprietary information of
 Jalasoft , Confidential Information "). You shall not
 disclose such Confidential Information and shall use it only in
 accordance with the terms of the license agreement you entered into
 with Jalasoft
*/


const expect = require("chai").expect;
const request = require('supertest');
const assert = require('assert');
const app = require('../../app');
const conn = require('../../index');
const project_id = '605b64bbbba34d23b8d0c338';


describe('file controller test', () => {

    it("save file with null file_name", (done) => {
        request(app).post('/api/v1/file')
        .send({
            file_name : null,
            project_id : project_id,
            content: 'test'
        })
        .then((res) => {
            const body = res.body;
            expect(body).to.contain.property('error');
            assert.deepStrictEqual(body['error'], 'File Name in null or empty');
            assert.deepStrictEqual(res.status, 400);
            done();
        }).catch((err) =>{});
    });

    it("save file with null values", (done) => {
        request(app).post('/api/v1/file')
        .send({
            file_name : 'mascota',
            project_id : null,
            content: 'test'
        })
        .then((res) => {
            const body = res.body;
            expect(body).to.contain.property('error');
            assert.deepStrictEqual(body['error'], 'Project Id in null or empty');
            assert.deepStrictEqual(res.status, 400);
            done();
        }).catch((err) =>{});
    });

    it("save file with null file_name", (done) => {
        request(app).post('/api/v1/file')
        .send({
            file_name : 'mascota',
            project_id : project_id,
            content: null
        })
        .then((res) => {
            const body = res.body;
            expect(body).to.contain.property('error');
            assert.deepStrictEqual(body['error'], 'Content in null or empty');
            assert.deepStrictEqual(res.status, 400);
            done();
        }).catch((err) =>{});
    });


    it("save file with incorrect project id", (done) => {
        request(app).post('/api/v1/file')
        .send({
            file_name : 'mascota',
            project_id : project_id+'11664',
            content: 'test'
        })
        .then((res) => {
            const body = res.body;
            expect(body).to.contain.property('error');
            assert.deepStrictEqual(body['error'], 'Project doesn\'t exist in database');
            assert.deepStrictEqual(res.status, 400);
            done();
        }).catch((err) =>{});
    });

    it("save file with correct values", (done) => {
        request(app).post('/api/v1/file')
        .send({
            file_name : 'mascota',
            project_id : project_id,
            content: 'test'
        })
        .then((res) => {
            const body = res.body;
            request(app).delete('/api/v1/file/' + body._id).catch((err) =>{});
            expect(body).to.contain.property('_id');
            expect(body).to.contain.property('file_name');
            expect(body).to.contain.property('project');
            assert.deepStrictEqual(res.status, 200);
            done();
        }).catch((err) =>{});
    });

    it("update file with incorrect values", (done) => {
        request(app).put('/api/v1/file/1111111')
        .send({
            content : 'testing'
        })
        .then((res) => {
            const body = res.body;
            expect(body).to.contain.property('error');
            assert.deepStrictEqual(body['error'], 'File doesn\'t exist in database');
            assert.deepStrictEqual(res.status, 400);
            done();
        }).catch((err) =>{});
    });

    it("update file with correct values", (done) => {
        request(app).post('/api/v1/file')
        .send({
            file_name : 'mascota',
            project_id : project_id,
            content: 'test'
        })
        .then((res) => {
            const body = res.body;
            request(app).put('/api/v1/file/'+body['_id'])
            .send({content : 'testing update'})
            .then((res) => {
                const body = res.body;
                request(app).delete('/api/v1/file/' + body._id).catch((err) =>{});
                expect(body).to.contain.property('_id');
                expect(body).to.contain.property('file_name');
                expect(body).to.contain.property('project');
                assert.deepStrictEqual(res.status, 200);
                done();
            }).catch((err) =>{});
        }).catch((err) =>{});  
    });

    it("delete file with incorrect values", (done) => {
        request(app).delete('/api/v1/file/11111')
        .then((res) => {
            const body = res.body;
            expect(body).to.contain.property('error');
            assert.deepStrictEqual(body['error'], 'File doesn\'t exist in database');
            assert.deepStrictEqual(res.status, 400);
            done();
        }).catch((err) =>{});  
    });

    it("get project files with incorrect id", (done) => {
        request(app).get('/api/v1/file/11111')
        .then((res) => {
            const body = res.body;
            expect(body).to.contain.property('error');
            assert.deepStrictEqual(body['error'], 'Project doesn\'t exist in database');
            assert.deepStrictEqual(res.status, 400);
            done();
        }).catch((err) =>{});  
    });

    it("get project files with correct id", (done) => {
        request(app).get('/api/v1/file/'+project_id)
        .then((res) => {
            const body = res.body;
            expect(body).to.be.an('array');
            assert.deepStrictEqual(res.status, 200);
            done();
        }).catch((err) =>{});  
    });
});
